#include <memory>
#include <stack>
#include <string>
#include <vector>
#include "token.h"
#include "expr.h"

IdentToken::IdentToken(std::string s) : Token(s) {}

void IdentToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
  out_list.push_back((*it));
}
void IdentToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
  std::shared_ptr<Token> e(new NumberToken(std::to_string(Memory::get_var(get_value()))));
  lit_stack.push(e);
}