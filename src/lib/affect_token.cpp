#include <iostream>
#include <memory>
#include <stack>
#include <vector>
#include "token.h"

AffectToken::AffectToken(std::string s) : Token(s) {}

void AffectToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
  auto t(*it);
  while (!op_stack.empty())
  {
    std::shared_ptr<Token> op = op_stack.top();
    op_stack.pop();
    out_list.push_back(op);
  }
  op_stack.push(t);
}
void AffectToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
  lit_stack.top()->affect(get_value());
/*  std::string name = value;
  name.erase(name.end() - 2, name.end());
  Memory::set_var(name, stod(lit_stack.top()->get_value()));
*/}
bool AffectToken::compare(std::shared_ptr<Token> e) const { return false; };