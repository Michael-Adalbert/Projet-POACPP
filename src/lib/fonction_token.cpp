#include <iostream>
#include <memory>
#include <functional>
#include <stack>
#include <map>
#include <vector>
#include "token.h"
#include "expr.h"

FonctionToken::FonctionToken(std::string s) : Token(s) {}

void FonctionToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
  std::vector<std::shared_ptr<Token>> sub_expr;
  auto it_sub_expr = it;
  auto parenthesis_close = 0;
  auto parameter = 1;
  auto fun_token = (*it);
  do
  {
    std::shared_ptr<Token> sub_tok = *it_sub_expr;
    if (sub_tok->get_value() == "(" || sub_tok->is_function())
    {
      if (parenthesis_close != 0)
      {
        sub_expr.push_back(sub_tok);
      }
      parenthesis_close++;
      it_sub_expr++;
    }
    else if (sub_tok->get_value() == ")")
    {
      parenthesis_close--;
      if (parenthesis_close != 0)
      {
        sub_expr.push_back(sub_tok);
        it_sub_expr++;
      }
    }
    else if (sub_tok->is_separator())
    {
      if (parenthesis_close == 1)
      {
        parameter++;
      }
      else
      {
        sub_expr.push_back(sub_tok);
      }
      it_sub_expr++;
    }
    else
    {
      sub_expr.push_back(sub_tok);
      it_sub_expr++;
    }
  } while (parenthesis_close > 0);

  std::stack<std::shared_ptr<Token>> sub_op_stack;
  std::vector<std::shared_ptr<Token>> sub_toks;
  for (auto sub_it = sub_expr.begin(); sub_it != sub_expr.end(); sub_it++)
  {
    auto t{*sub_it};
    t->produce_rpn(sub_op_stack, sub_toks, sub_it);
  }
  while (!sub_op_stack.empty())
  {
    auto t = sub_op_stack.top();
    sub_op_stack.pop();
    sub_toks.push_back(t);
  }
  out_list.insert(out_list.end(), sub_toks.begin(), sub_toks.end());
  it = it_sub_expr;
  fun_token->set_num_param(parameter);
  out_list.push_back(fun_token);
}

void FonctionToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
  std::string nom_fonction = get_value();
  nom_fonction.erase(nom_fonction.end() - 1);
  std::vector<double> parametres;
  std::vector<int> placeholders;
  std::shared_ptr<Token> a;
  if (Fonction::fixed_func.find(nom_fonction) != Fonction::fixed_func.end())
  {
    for (auto i = 0; i < nb_param; i++)
    {
      std::shared_ptr<Token> parametre(lit_stack.top());
      lit_stack.pop();
      parametre->get_param(parametres, placeholders, (nb_param - 1) - i);
    }
    if (parametres.size() < Fonction::fixed_func[nom_fonction].first)
    {
      int id_partial_func = Fonction::get_id_fonction();
      Fonction::partial_func[id_partial_func] = std::make_tuple(Fonction::fixed_func[nom_fonction].first - parametres.size(), parametres, placeholders, nom_fonction);
      std::shared_ptr<Token> f(new FonctionToken(std::to_string(id_partial_func)));
      a = f;
    }
    else
    {
      double r = Fonction::fixed_func[nom_fonction].second(parametres);
      std::shared_ptr<Token> f(new NumberToken(std::to_string(r)));
      a = f;
    }
  }
  else if (Fonction::variable_func.find(nom_fonction) != Fonction::variable_func.end())
  {

    for (auto i = 0; i < nb_param; i++)
    {
      std::shared_ptr<Token> parametre(lit_stack.top());
      lit_stack.pop();
      parametre->get_param(parametres, placeholders, (nb_param - 1) - i);
    }
    if (parametres.size() < parametres[0] + 3)
    {
      int id_partial_func = Fonction::get_id_fonction();
      Fonction::partial_func[id_partial_func] = std::make_tuple((parametres[0] + 3) - parametres.size(), parametres, placeholders, nom_fonction);
      std::shared_ptr<Token> f(new FonctionToken(std::to_string(id_partial_func)));
      a = f;
    }
    else
    {
      double r = Fonction::fixed_func[nom_fonction].second(parametres);
      std::shared_ptr<Token> f(new NumberToken(std::to_string(r)));
      a = f;
    }
  }
  else if (Fonction::association_nom_fonction.find(nom_fonction) != Fonction::association_nom_fonction.end())
  {
    //Si on appel une fonction partiel
    int nombre_param_restant;
    std::vector<double> param;
    std::vector<int> ph;
    std::string fonction;
    std::tie(nombre_param_restant, param, ph, fonction) = Fonction::partial_func[Fonction::association_nom_fonction[nom_fonction]];
    for (auto i = 0; i < nombre_param_restant; i++)
    {
      std::shared_ptr<Token> parametre(lit_stack.top());
      lit_stack.pop();
      if (ph.size() > i)
      {
        param.insert(param.begin() + ph[i], stod(parametre->get_value()));
      }
      else
      {
        parametre->get_param(parametres, placeholders, i);
      }
    }

    parametres.insert(parametres.begin(), param.begin(), param.end());

    double r;
    if (Fonction::fixed_func.find(fonction) != Fonction::fixed_func.end())
    {
      r = Fonction::fixed_func[fonction].second(parametres);
    }
    else
    {
      r = Fonction::variable_func[fonction](parametres);
    }
    std::shared_ptr<Token> f(new NumberToken(std::to_string(r)));
    a = f;
  }
  lit_stack.push(a);
}

void FonctionToken::affect(std::string s) const
{
  s.erase(s.end() - 1);
  while (std::isspace((s[s.length() - 1])))
  {
    s.erase(s.end() - 1);
  }
  Fonction::association_nom_fonction[s] = stoi(get_value());
}

/*
Fonction::Fonction(const std::string &n) : name(n) {}
double Fonction::eval() const {
  if (fixed_func.find(name) != fixed_func.end()) {
    return fixed_func[name].second(param);
  } else {
    return 0;
  }
}
void Fonction::print() const {
  std::cout << name << "(";
  for (auto it = param.begin(); it < param.end(); it++) {
    auto p(*it);
    p->print();
    if (it + 1 != param.end()) {
      std::cout << ",";
    }
  }
  std::cout << ")";
}
*/
