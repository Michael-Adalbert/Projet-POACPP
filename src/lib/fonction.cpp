#include <functional>
#include <map>
#include <string>
#include <tuple>
#include <vector>
#include <cmath>
#include "expr.h"

Fonction::fonction_fixee Fonction::fixed_func = {
    {"sin", std::make_pair(1, [](const Fonction::parametre &p) { return sinf64(p[0]); })},
    {"cos", std::make_pair(1, [](const Fonction::parametre &p) { return cosf64(p[0]); })},
    {"tan", std::make_pair(1, [](const Fonction::parametre &p) { return tanf64(p[0]); })},
    {"sqrt", std::make_pair(1, [](const Fonction::parametre &p) { return sqrtf64(p[0]); })},
    {"log", std::make_pair(1, [](const Fonction::parametre &p) { return log(p[0]); })},
    {"exp", std::make_pair(1, [](const Fonction::parametre &p) { return exp(p[0]); })},
    {"pow", std::make_pair(2, [](const Fonction::parametre &p) { return pow(p[0], p[1]); })},
    {"hypot", std::make_pair(2, [](const Fonction::parametre &p) {
         double a = p[0];
         double b = p[1];
         return hypot(a, b);
     })},
    {"lerp", std::make_pair(3, [](const Fonction::parametre &p) {
         double xa = p[0];
         double xb = p[1];
         double e = p[2];
         return (xa + (xb - xa) * e);
     })}};

Fonction::fonction_variable Fonction::variable_func = {
    {"polynome", [](const Fonction::parametre &p) {
         double degree = p[0];
         double sum = 0;
         double x = p[p.size() - 1];
         for (auto index = p.size() - 2; index > 0; index--)
         {
             double coef = p[index];
             sum += coef * pow(x, degree);
             degree--;
         }
         return sum;
     }}};
Fonction::fonction_partiel Fonction::partial_func = {};
Fonction::function_name Fonction::association_nom_fonction = {};

int Fonction::id_fonction = 1;

int Fonction::get_id_fonction()
{
    int i = id_fonction++;
    return i;
}