#include <iostream>
#include <map>
#include <stack>
#include <vector>
#include "token.h"

std::map<std::string, int> priority_operator_map = {
    {"+", 0}, {"-", 0}, {"*", 1}, {"/", 1}};

OperatorToken::OperatorToken(std::string s) : Token(s) {}

void OperatorToken::produce_rpn(
    std::stack<std::shared_ptr<Token>> &op_stack,
    std::vector<std::shared_ptr<Token>> &out_list,
    std::vector<std::shared_ptr<Token>>::iterator &it) const
{
  auto t(*it);
  while ((!op_stack.empty()) ? op_stack.top()->compare(t) : false)
  {
    std::shared_ptr<Token> op = op_stack.top();
    op_stack.pop();
    out_list.push_back(op);
  }
  op_stack.push(t);
}
void OperatorToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
  std::shared_ptr<Token> right = lit_stack.top();
  lit_stack.pop();
  std::shared_ptr<Token> left = lit_stack.top();
  lit_stack.pop();
  auto l = stod(left->get_value()), r = stod(right->get_value());
  double e;
  if (get_value() == "+")
    e = l + r;
  else if (get_value ()== "-")
    e = l - r;
  else if (get_value() == "*")
    e = l * r;
  else if (get_value() == "/")
    e = l / r;
  else
    throw std::exception();
  std::shared_ptr<Token> shrd_tok(new NumberToken(std::to_string(e)));
  lit_stack.push(shrd_tok);
}
bool OperatorToken::compare(std::shared_ptr<Token> e) const
{
  if (priority_operator_map.find(e->get_value()) !=
      priority_operator_map.end())
  {
    int prio1(priority_operator_map[get_value()]);
    int prio2(priority_operator_map[e->get_value()]);
    return (prio1 >= prio2);
  }
  else
  {
    return false;
  }
}
