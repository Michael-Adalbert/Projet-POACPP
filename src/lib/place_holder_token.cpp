#include <vector>
#include <stack>
#include <string>
#include "token.h"
#include "expr.h"

PlaceHolderToken::PlaceHolderToken(std::string s)
    : Token(s)
{
}

void PlaceHolderToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
    out_list.push_back((*it));
}
void PlaceHolderToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
    std::shared_ptr<Token> a(new PlaceHolderToken(get_value()));
    lit_stack.push(a);
}
void PlaceHolderToken::get_param(std::vector<double> &parametres, std::vector<int> &placeholders, int place) const
{
    placeholders.push_back(place);
}