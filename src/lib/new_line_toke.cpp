#include <iostream>
#include <memory>
#include <stack>
#include <vector>

#include "token.h"

bool NewLineToken::print = true;

NewLineToken::NewLineToken(std::string s) : Token(s)
{
    NewLineToken::print = true;
}

void NewLineToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
    auto t(*it);
    while (!op_stack.empty())
    {
        std::shared_ptr<Token> op = op_stack.top();
        op_stack.pop();
        out_list.push_back(op);
    }
    out_list.push_back(t);
}
void NewLineToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
    if (NewLineToken::print)
        std::cout << lit_stack.top()->get_value() << std::endl;
}
bool NewLineToken::compare(std::shared_ptr<Token> e) const { return false; }