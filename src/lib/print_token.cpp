#include <iostream>
#include <memory>
#include <stack>
#include <vector>
#include "token.h"

PrintToken::PrintToken(std::string s) : Token(s) {}

void PrintToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
    auto t(*it);
    while (!op_stack.empty())
    {
        std::shared_ptr<Token> op = op_stack.top();
        op_stack.pop();
        out_list.push_back(op);
    }
    op_stack.push(t);
}
void PrintToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
    NewLineToken::print = false;
}

bool PrintToken::compare(std::shared_ptr<Token> e) const { return false; }