#include "token.h"

SeparatorToken::SeparatorToken()
    : Token(",")
{
}

void SeparatorToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
}
void SeparatorToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
}