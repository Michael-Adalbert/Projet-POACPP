#include <iostream>
#include <memory>
#include <regex>
#include <string>
#include <vector>
#include "token.h"
#include "expr.h"

const std::regex GLOBAL(
    "("
    "(_[1-9])|"
    "(;)|"
    "([\n])|"
    "([A-Za-z][A-Za-z0-9]*[ ]*=)|"
    "(,)|"
    "([A-Za-z][A-Za-z0-9]*\\()|"
    "([A-Za-z][A-Za-z0-9]*)|"
    "(([0]|[1-9][0-9]*)(\\.[0-9]*)?)|"
    "(\\+|-|/|\\*)|"
    "(\\(|\\))"
    ")");
const std::regex OPEN_PARANTHESES("\\(");
const std::regex CLOSE_PARANTHESES("\\)");
const std::regex NUMBER("([0]|[1-9][0-9]*)");
const std::regex OPERATOR("(\\+|-|/|\\*)");
const std::regex IDENT("[A-Za-z][A-Za-z0-9]*");
const std::regex FUNCTION_CALL("[A-Za-z][A-Za-z0-9]*\\(");
const std::regex SEPARATOR(",");
const std::regex AFFECT("[A-Za-z][A-Za-z0-9]*[ ]*=");
const std::regex PRINT(";");
const std::regex NEWLINE("[\n]");
const std::regex PLACEHOLDER("_[1-9]");

void Expr::lexer(const std::string &s)
{
    // bool permetant de savoir si on est dans une nouvelle expression ou pas
    // Exemple : 3 + - 4 | -4 est une nouvel expression car apres un operateur
    bool begin_new_exp = true;
    std::string base(s);
    std::smatch m;
    // chaque valeur de token est mit à la suite du tableau
    while (regex_search(base, m, GLOBAL))
    {
        std::smatch tmp;
        std::string t(m[0]);
        if (regex_search(t, tmp, PLACEHOLDER))
        {
            infix.emplace_back(new PlaceHolderToken(t));
        }
        else if (regex_search(t, tmp, PRINT))
        {
            infix.emplace_back(new PrintToken(t));
        }
        else if (regex_search(t, tmp, AFFECT))
        {
            infix.emplace_back(new AffectToken(t));
            begin_new_exp = true;
        }
        else if (regex_search(t, tmp, FUNCTION_CALL))
        {
            infix.emplace_back(new FonctionToken(t));
            begin_new_exp = true;
        }
        else if (regex_search(t, tmp, SEPARATOR))
        {
            infix.emplace_back(new SeparatorToken());
            begin_new_exp = true;
        }
        else if (regex_search(t, tmp, OPEN_PARANTHESES))
        {
            infix.emplace_back(new ParanthesesToken("("));
            begin_new_exp = true;
        }
        else if (regex_search(t, tmp, CLOSE_PARANTHESES))
        {
            infix.emplace_back(new ParanthesesToken(")"));
        }
        else if (regex_search(t, tmp, IDENT))
        {
            infix.emplace_back(new IdentToken(t));
            begin_new_exp = false;
        }
        else if (regex_search(t, tmp, NUMBER))
        {
            infix.emplace_back(new NumberToken(t));
            begin_new_exp = false;
        }
        else if (regex_search(t, tmp, OPERATOR))
        {
            if (begin_new_exp)
            {
                if (t[0] == '-')
                {
                    infix.emplace_back(new NumberToken("-1"));
                    infix.emplace_back(new OperatorToken("*"));
                }
            }
            else
            {
                infix.emplace_back(new OperatorToken(t));
            }
            begin_new_exp = true;
        }
        else
        {
            throw std::exception();
        }
        base = m.suffix().str();
    }
    infix.emplace_back(new NewLineToken("\n"));
}

void Expr::parser()
{
    std::stack<std::shared_ptr<Token>> operator_stack;
    for (auto it = infix.begin(); it != infix.end(); it++)
    {
        auto t = it->get();
        t->produce_rpn(operator_stack, rpn, it);
    }
}

void Expr::eval()
{
    std::stack<std::shared_ptr<Token>> literal_stack;
    for (auto t : rpn)
    {
        t->eval(literal_stack);
    }
}
Expr::Expr(std::string str)
{
    lexer(str);
    parser();
    eval();
}
