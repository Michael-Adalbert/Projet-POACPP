#include <iostream>
#include <iterator>
#include <memory>
#include <regex>
#include <string>
#include <vector>
#include "expr.h"
#include "program.h"

void Program::start() {
  std::cout.precision(12);
  std::string line;
  do {
    std::getline(std::cin, line, '\n');

    if (!std::cin.eof() || !line.empty()) {
      if (!line.empty()) {
        Expr e(line);
      }
    }
  } while (!std::cin.eof());
}