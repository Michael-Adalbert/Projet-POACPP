#include <vector>
#include <stack>
#include <iostream>
#include "token.h"
#include "expr.h"

ParanthesesToken::ParanthesesToken(std::string s)
    : Token(s)
{
}

void ParanthesesToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
    std::vector<std::shared_ptr<Token>> sub_expr;
    auto it_sub_expr = it;
    auto parenthesis_close = 0;

    do
    {
        std::shared_ptr<Token> sub_tok = *it_sub_expr;
        if (sub_tok->get_value() == "(" || sub_tok->is_function())
        {
            if (parenthesis_close != 0)
            {
                sub_expr.push_back(sub_tok);
            }
            parenthesis_close++;
            it_sub_expr++;
        }
        else if (sub_tok->get_value() == ")")
        {
            parenthesis_close--;
            if (parenthesis_close != 0)
            {
                sub_expr.push_back(sub_tok);
                it_sub_expr++;
            }
        }
        else
        {
            sub_expr.push_back(sub_tok);
            it_sub_expr++;
        }
    } while (parenthesis_close > 0);

    std::stack<std::shared_ptr<Token>> sub_op_stack;
    std::vector<std::shared_ptr<Token>> sub_toks;
    for (auto sub_it = sub_expr.begin(); sub_it != sub_expr.end(); sub_it++)
    {
        auto t{*sub_it};
        t->produce_rpn(sub_op_stack, sub_toks, sub_it);
    }
    while (!sub_op_stack.empty())
    {
        auto t = sub_op_stack.top();
        sub_op_stack.pop();
        sub_toks.push_back(t);
    }
    out_list.insert(out_list.end(), sub_toks.begin(), sub_toks.end());
    it = it_sub_expr;
}
void ParanthesesToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
}