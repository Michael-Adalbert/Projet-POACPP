#include <string>
#include <map>
#include "expr.h"

Memory::memoire Memory::mem = {
    {"pi", 3.1415926535897932385}};

void Memory::set_var(std::string s, double value) { Memory::mem[s] = value; }

double Memory::get_var(std::string s) { return Memory::mem[s]; }

bool Memory::var_is_exist(std::string name)
{
    return Memory::mem.find(name) != Memory::mem.end();
}
