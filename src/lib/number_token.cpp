#include <vector>
#include <stack>
#include <string>
#include "token.h"
#include "expr.h"

NumberToken::NumberToken(std::string s)
    : Token(s)
{
}

void NumberToken::produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const
{
    out_list.push_back((*it));
}
void NumberToken::eval(std::stack<std::shared_ptr<Token>> &lit_stack) const
{
    auto val = stod(get_value());
    std::shared_ptr<Token> a(new NumberToken(std::to_string(val)));
    lit_stack.push(a);
}
void NumberToken::get_param(std::vector<double> &parametres,std::vector<int> &placeholders,int place) const
{
    parametres.insert(parametres.begin(), stod(get_value()));
}

void NumberToken::affect(std::string s) const
{
    s.erase(s.end() - 1);
    while (std::isspace((s[s.length() - 1])))
    {
        s.erase(s.end() - 1);
    }
    Memory::set_var(s,stod(get_value()));
}
