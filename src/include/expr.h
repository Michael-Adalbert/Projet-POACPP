#pragma once

#include <functional>
#include <map>
#include <string>
#include <tuple>
#include <vector>
#include "token.h"

// Expr
class Expr
{
private:
  using token = std::shared_ptr<Token>;
  std::vector<token> infix;
  std::vector<token> rpn;

public:
  Expr(std::string str);
  void lexer(const std::string &s);
  void parser();
  void eval();
};

class Fonction
{
private:
  using parametre = std::vector<double>;
  using placeholder = std::vector<int>;
  using fonction_fixee = std::map<std::string, std::pair<int, std::function<double(parametre)>>>;
  using fonction_variable = std::map<std::string, std::function<double(parametre)>>;
  using fonction_partiel = std::map<int, std::tuple<int, parametre, placeholder, std::string>>;
  using function_name = std::map<std::string, int>;
  static int id_fonction;

public:
  static fonction_fixee fixed_func;
  static fonction_variable variable_func;
  static fonction_partiel partial_func;
  static function_name association_nom_fonction;
  static int get_id_fonction();
};
// Memory
class Memory
{
public:
  static void set_var(std::string s, double value);
  static double get_var(std::string s);
  static bool var_is_exist(std::string name);

private:
  Memory(){};
  using memoire = std::map<std::string, double>;
  static memoire mem;
};
