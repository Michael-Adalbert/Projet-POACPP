#pragma once

#include <memory>
#include <stack>
#include <string>
#include <vector>
#include "token.h"
class Token
{
private:
    std::string value;

public:
    Token(std::string s) : value(s){};
    virtual ~Token() {}
    virtual bool compare(std::shared_ptr<Token> e) const { return false; };
    virtual std::string get_value() const { return value; };
    virtual void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const = 0;
    virtual void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const = 0;
    virtual void set_num_param(int n){};
    virtual int get_num_param() const { return 0; };
    virtual bool is_function() const { return false; };
    virtual bool is_separator() const { return false; };
    virtual void get_param(std::vector<double> &parametres, std::vector<int> &placeholders, int place) const {};
    virtual void affect(std::string name) const {};
};
class PlaceHolderToken : public Token
{
public:
    PlaceHolderToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    void get_param(std::vector<double> &parametres, std::vector<int> &placeholders, int place) const override;
};
class NumberToken : public Token
{
public:
    NumberToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    void get_param(std::vector<double> &parametres, std::vector<int> &placeholders, int place) const override;
    void affect(std::string name) const override;
};
class OperatorToken : public Token
{
public:
    OperatorToken(std::string s);
    bool compare(std::shared_ptr<Token> e) const override;
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
};
class ParanthesesToken : public Token
{
public:
    ParanthesesToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
};
class IdentToken : public Token
{
public:
    IdentToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
};
class FonctionToken : public Token
{
public:
    FonctionToken(std::string s);
    void set_num_param(int n) override { nb_param = n; };
    int get_num_param() const override { return nb_param; };
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    bool is_function() const override { return true; }
    void affect(std::string name) const override;

private:
    int nb_param = 1;
};
class AffectToken : public Token
{
public:
    AffectToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    bool compare(std::shared_ptr<Token> e) const override;
};
class PrintToken : public Token
{
public:
    PrintToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    bool compare(std::shared_ptr<Token> e) const override;
};
class NewLineToken : public Token
{
public:
    static bool print;
    NewLineToken(std::string s);
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    bool compare(std::shared_ptr<Token> e) const override;
};
class SeparatorToken : public Token
{
public:
    SeparatorToken();
    void produce_rpn(std::stack<std::shared_ptr<Token>> &op_stack, std::vector<std::shared_ptr<Token>> &out_list, std::vector<std::shared_ptr<Token>>::iterator &it) const override;
    void eval(std::stack<std::shared_ptr<Token>> &lit_stack) const override;
    bool is_separator() const override { return true; }
};
